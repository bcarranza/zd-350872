const express = require('express')
const app = express()
const port = 3000

// Database connection
mongoose.connect('mongodb://127.0.0.1:27017/example', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});
  
// User model
const User = mongoose.model('User', {
    name: { type: String },
    age: { type: Number }
});

// Untrusted user input
app.post('*', (req, res) => {
  User.findOne({
    where: {
      age: req.body
    }
  })
  res.json(req.body);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

// Improper control of generation of code (code injection)
function handler(userInput) {
    var anyVal = 'anyVal';
    user[anyVal] = user[userInput[0]](userInput[1]);
}


